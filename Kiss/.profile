# KISS COMPILE CONFIGS
export CFLAGS="-march=x86-64 -mtune=generic -pipe -Os"
export CXXFLAGS="$CFLAGS"
export MAKEFLAGS="-j16"

# KISS REPOS
export KISS_PATH=''
KISS_PATH=/var/db/kiss/repos/repo/core
KISS_PATH=/var/db/kiss/repos/repo/extra
KISS_PATH=/var/db/kiss/repos/repo/wayland
KISS_PATH=/var/db/kiss/repos/community/community
KISS_PATH=/var/db/kiss/repos/kiss-dbus/dbus
KISS_PATH=$KISS_PATH:/var/db/kiss/repos/repo/core
KISS_PATH=$KISS_PATH:/var/db/kiss/repos/repo/extra
KISS_PATH=$KISS_PATH:/var/db/kiss/repos/repo/wayland
KISS_PATH=$KISS_PATH:/var/db/kiss/repos/community/community
KISS_PATH=$KISS_PATH:/var/db/kiss/repos/kiss-dbus/dbus

# XDG_RUNTIME_DIR
if [ -z "$XDG_RUNTIME_DIR" ]; then
	XDG_RUNTIME_DIR="/tmp/$(id -u)-runtime-dir"

	mkdir -pm 0700 "$XDG_RUNTIME_DIR"
	export XDG_RUNTIME_DIR
fi

# FIREFOX WAYLAND CONFIG
export MOZ_ENABLE_WAYLAND=1
export MOZ_WAYLAND_DRM_DEVIE=/dev/dri/renderD128

# HYBRID GRAPHICS
#export env DRI_PRIME=1

# ASHRC
export ENV=~/.ashrc

# NODEJS NPM BINARIES
export PATH="$PATH:$HOME/.npm/bin"

# PYTHON PIP BINARIES
export PATH="${PATH}:${HOME}/.local/bin"

# GO BINARIES
export GOPATH=$HOME/.go
export PATH="$PATH:$HOME/.go/bin"

export XDG_CURRENT_DESKTOP=wlroots
export XDG_SESSION_DESKTOP=wlroots

# AUTO INIT
if [ -z $DISPLAY ] && [ $(tty) = /dev/tty1 ]; then
  dbus-run-session dwl -s 'dbus-update-activation-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=wlroots & pipewire & pipewire-pulse & foot -s <&-' 2> /home/asimov/test.log
fi
