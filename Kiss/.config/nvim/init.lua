local opt = vim.opt
local o = vim.o
local g = vim.g

-------------------------------------- options ------------------------------------------
o.laststatus = 3
o.showmode = false

o.clipboard = "unnamedplus"
o.cursorline = true
o.cursorlineopt = "number"

-- Indenting
o.expandtab = true
o.shiftwidth = 2
o.smartindent = true
o.tabstop = 2
o.softtabstop = 2

opt.fillchars = { eob = " " }
o.ignorecase = true
o.smartcase = true
o.mouse = "a"

-- Numbers
o.number = true
o.numberwidth = 2
o.ruler = false

-- disable nvim intro
opt.shortmess:append "sI"

o.signcolumn = "yes"
o.splitbelow = true
o.splitright = true
o.timeoutlen = 400
o.undofile = true

-- interval for writing swap file to disk, also used by gitsigns
o.updatetime = 250

-- go to previous/next line with h,l,left arrow and right arrow
-- when cursor reaches end/beginning of line
opt.whichwrap:append "<>[]hl"

require("bufferline").setup({
	options = {
		offsets = {
			{
				filetype = "NvimTree",
            	text = "File Explorer",
				separator = true,
        	}
		},
	}
})
require('nvim-autopairs').setup()
require("neopywal").setup({
    -- Uses a template file `~/.cache/wallust/colors_neopywal.vim` instead of the regular
    -- pywal template at `~/.cache/wal/colors-wal.vim`
    use_wallust = false,

    -- Sets the background color of certain highlight groups to be transparent.
    -- Use this when your terminal opacity is < 1.
    transparent_background = true,

    -- With this option you can overwrite all the base colors the colorscheme uses.
    custom_colors = {},

    -- With this option you can overwrite any highlight groups set by the colorscheme.
    custom_highlights = {},

    -- Dims the background when another window is focused.
    dim_inactive = true,

    -- Apply colorscheme for Neovim's terminal (e.g. `g:terminal_color_0`).
    terminal_colors = true,

    -- Shows the '~' characters after the end of buffers.
    show_end_of_buffer = false,

    no_italic = false, -- Force no italic.
    no_bold = false, -- Force no bold.
    no_underline = false, -- Force no underline.
    no_undercurl = false, -- Force no undercurl.
    no_strikethrough = false, -- Force no strikethrough.

    -- Handles the styles of general hi groups (see `:h highlight-args`).
    styles = {
        comments = { "italic" },
        conditionals = { "italic" },
        loops = {},
        functions = {},
        keywords = {},
        includes = { "italic" },
        strings = {},
        variables = { "italic" },
        numbers = {},
        booleans = {},
        -- properties = {},
        types = { "italic" },
        operators = {},
    },

    -- Setting this to false disables all default file format highlights.
    -- Useful if you want to enable specific file format options.
    default_fileformats = true,

    -- Setting this to false disables all default plugin highlights.
    -- Useful if you want to enable specific plugin options.
    default_plugins = true,

    fileformats = {
        c_cpp = true,
        clojure = true,
        cmake = true,
        git_commit = true,
        c_sharp = true,
        css = true,
        dart = true,
        diff = true,
        elixir = true,
        erlang = true,
        go = true,
        haskell = true,
        help = true,
        html = true,
        ini = true,
        java = true,
        json = true,
        javascript_react = true,
        javascript = true,
        kotlin = true,
        latex = true,
        less = true,
        common_lisp = true,
        lua = true,
        makefile = true,
        markdown = true,
        matlab = true,
        objectivec = true,
        ocaml = true,
        perl = true,
        php = true,
        powershell = true,
        python = true,
        restructuredtext = true,
        ruby = true,
        rust = true,
        sass = true,
        scala = true,
        shell = true,
        swift = true,
        toml = true,
        typescript = true,
        viml = true,
        xml = true,
        yaml = true,
        zsh = true,
    },

    plugins = {
        notify = true,
        noice = true,
        ale = true,
        alpha = true,
        nvim_cmp = true,
        lazygit = true,
        coc = true,
        dashboard = true,
        git_gutter = true,
        indent_blankline = true,
        lazy = true,
        lspconfig = true,
        neotree = true,
        netrw = true,
        telescope = true,
        treesitter = true,
        undotree = true,
        which_key = true,
        mini = {
            cursorword = true,
            files = true,
            hipatterns = true,
            indentscope = true,
            pick = true,
            starter = true,
            statusline = true,
            tabline = true,
        },
    },
})
vim.cmd.colorscheme("neopywal")
require('lualine').setup({

  options= {
  	theme= 'neopywal',
  },
})
require('nvim-web-devicons').setup()
require('nvim-tree').setup({
	renderer = {
		indent_markers = {
    		enable = true,
		},

		icons = {
        		git_placement = "signcolumn",
        		show = {
          		file = true,
          		folder = false,
          		folder_arrow = false,
          		git = true,
        		},
		},

	},
})
require'nvim-treesitter.configs'.setup {

  -- A list of parser names, or "all"
  ensure_installed = { "c", "lua", "cpp", "java", "bash", "html", "javascript", "angular", "ssh_config"},

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  highlight = {
    enable = true,
    use_languagetree = true,
    additional_vim_regex_highlighting = false,
  },

  ident = { enable = true },
  

  autotag = {
    enable = true,
    enable_rename = true,
    enable_close = true,
    enable_close_on_slash = true,
    filetypes = { "html" , "xml" },
  }
}
