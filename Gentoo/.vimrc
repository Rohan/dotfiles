set number

filetype on
syntax on

colorscheme wal

" Lightline

set laststatus=2
set noshowmode

let g:lightline = {
      	\ 'colorscheme': 'wal',
	\ }
