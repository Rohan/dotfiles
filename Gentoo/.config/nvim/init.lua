vim.o.number = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4

require("bufferline").setup({
	options = {
		offsets = {
			{
				filetype = "NvimTree",
            	text = "File Explorer",
				separator = true,
        	}
		},
	}
})
require('nvim-autopairs').setup()
require('pywal16').setup()
require('lualine').setup({

	options= {
		theme= 'pywal16-nvim',
	},
})
require('nvim-web-devicons').setup()
require('nvim-tree').setup({
	renderer = {
		indent_markers = {
    		enable = true,
		},

		icons = {
        		git_placement = "signcolumn",
        		show = {
          		file = true,
          		folder = false,
          		folder_arrow = false,
          		git = true,
        		},
		},

	},
})
require'nvim-treesitter.configs'.setup {

  -- A list of parser names, or "all"
  ensure_installed = { "c", "lua", "cpp", "java", "bash", "html"},

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- List of parsers to ignore installing (for "all")
  ignore_install = { "javascript" },

  highlight = {
    -- `false` will disable the whole extension
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    disable = { "c", "bash" },

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
  
  autotag = {
    enable = true,
    enable_rename = true,
    enable_close = true,
    enable_close_on_slash = true,
    filetypes = { "html" , "xml" },
  }
}
