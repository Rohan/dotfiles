bluez_monitor.enabled = true

bluez_monitor.properties = {
  ["bluez5.enable-sbc-xq"] = false,
  ["bluez5.enable-msbc"] = false,
  ["bluez5.enable-ldac"] = true,
  ["bluez5.codecs"] = "[ldac]",
}
