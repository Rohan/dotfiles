# /etc/skel/.bash_profile

# This file is sourced by bash for login shells.  The following line
# runs your .bashrc and is recommended by the bash info pages.
if [[ -f ~/.bashrc ]] ; then
	. ~/.bashrc
fi

if test -z "${XDG_RUNTIME_DIR}"; then
	export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
	if ! test -d "${XDG_RUNTIME_DIR}"; then
		mkdir "${XDG_RUNTIME_DIR}"
		chmod 0700 "${XDG_RUNTIME_DIR}"
	fi
fi

export MOZ_ENABLE_WAYLAND=1
export GDK_BACKEND="wayland"

export _JAVA_AWT_WM_NONREPARENTING=1
export XCURSOR_SIZE=24

#NPM BINARIES
export PATH=~/.npm/bin:$PATH

#PIP BINARIES
export PATH="${PATH}:${HOME}/.local/bin/"

if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
  dbus-run-session dwl -s 'foot -s & gentoo-pipewire-launcher & wbg /path/wallpaper <&-'
fi
